#!/usr/bin/python3

from joystick import Joystick, JoystickListener
import motor
import steering
from camera import CameraControl
from lights import LightControl


class Car(JoystickListener):

	def __init__(self, debug=False):
		self.debug = debug

		motor.setup()
		self.speed = 0.0
		self.speed_forward = 0.0
		self.speed_backward = 0.0
		steering.setup()
		self.steering = 0.0
		self.camera = CameraControl()
		self.camera_inversion = 1
		self.camera_x = 0.0
		self.camera_y = 0.0
		self.camera_sticky = False

		self.laser = LightControl()

		self.reset()

		self.joystick = Joystick.singleton(False)
		try:
			self.joystick and self.joystick.safe_loop(self)
		except Exception as e:
			print(e)
			self.reset()

	def print(self, msg):
		self.debug and print(msg)

	def reset(self):
		self.drive(0.0, 0.0)

		self.camera.turn_xy_angle(0.0, 0.0)

		self.laser.switch(False)

	def drive(self, speed=0.0, angle=0.0):
		self.speed = speed

		differential_speed = self.speed * (1 - 0.1 * abs(angle))
		self.print('speed: %f, dif_speed: %f, angle: %.3f' % (self.speed, differential_speed, angle))
		motor.set_direction_speed_split(
			differential_speed if angle >= 0.0 else self.speed,
			differential_speed if angle <= 0.0 else self.speed)
		self.steering = angle
		steering.steer(self.steering)
		if self.camera_sticky:
			self.turn_camera(self.camera_x, self.camera_y)

	def turn_camera(self, camera_x=0.0, camera_y=0.0):
		self.camera_x = camera_x
		self.camera_y = camera_y
		x = self.steering * 0.3 + camera_x if self.camera_sticky else camera_x
		self.camera.turn_xy_angle(x * -1, camera_y)

	def on_button(self, button, is_pressed):
		if button == 'LOGO' and is_pressed:
			self.print('Reset')
			self.reset()
		if button == 'R3' and is_pressed:
			self.print('Inverse camera controls: %d' % self.camera_inversion)
			self.camera_inversion *= -1
		if button == 'L3' and is_pressed:
			self.camera_sticky = not self.camera_sticky
			self.print('Sticky camera controls: %s' % 'on' if self.camera_sticky else 'off')
		self.turn_camera(self.camera_x, self.camera_y)

		self.print("Car.on_button: %s %s" % (button, 'pressed' if is_pressed else 'released'))

		if button == 'A' and is_pressed:
			self.laser.toggle()

		if button == 'BACK' and is_pressed:
			self.reset()
			exit(0)

	def on_axis(self, axis, value):
		pass  # self.print("Car.on_axis: %s %.3f" % (axis, value))
		if axis == 'R2':
			self.speed_forward = value
		if axis == 'L2':
			self.speed_backward = value
		if axis == 'LX':
			self.steering = value

		if axis == 'RX':
			self.camera_x = value
		if axis == 'RY':
			self.camera_y = value * self.camera_inversion

		self.drive(self.speed_forward - self.speed_backward, self.steering)
		if not self.camera_sticky:
			self.turn_camera(self.camera_x, self.camera_y)

	def on_tick(self):
		pass

if __name__ == '__main__':
	car = Car(False)

