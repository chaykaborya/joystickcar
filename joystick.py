#!/usr/bin/python3

# Released by rdb under the Unlicense (unlicense.org)
# Based on information from:
# https://www.kernel.org/doc/Documentation/input/joystick-api.txt

import array
import os
import struct
from abc import abstractmethod
from fcntl import ioctl


class JoystickListener:

	@abstractmethod
	def on_button(self, button, is_pressed):
		"""button is_pressed"""

	@abstractmethod
	def on_axis(self, axis, value):
		"""axis value"""

	@abstractmethod
	def on_tick(self):
		"""each tick"""


class Joystick:

	instance = None

	# These constants were borrowed from linux/input.h
	axis_names = {
		0x00: 'LX',
		0x01: 'LY',
		0x02: 'L2',
		0x03: 'RX',
		0x04: 'RY',
		0x05: 'R2',
		0x06: 'throttle',
		0x07: 'rudder',
		0x08: 'wheel',
		0x09: 'gas',
		0x0a: 'brake',
		0x10: 'MX',
		0x11: 'MY',
		0x12: 'hat1_x',
		0x13: 'hat1_y',
		0x14: 'hat2_x',
		0x15: 'hat2_y',
		0x16: 'hat3_x',
		0x17: 'hat3_y',
		0x18: 'pressure',
		0x19: 'distance',
		0x1a: 'tilt_x',
		0x1b: 'tilt_y',
		0x1c: 'tool_width',
		0x20: 'volume',
		0x28: 'misc',
	}

	button_names = {
		0x120: 'trigger',
		0x121: 'thumb',
		0x122: 'thumb2',
		0x123: 'top',
		0x124: 'top2',
		0x125: 'pinkie',
		0x126: 'base',
		0x127: 'base2',
		0x128: 'base3',
		0x129: 'base4',
		0x12a: 'base5',
		0x12b: 'base6',
		0x12f: 'dead',
		0x130: 'A',
		0x131: 'B',
		0x132: 'c',
		0x133: 'X',
		0x134: 'Y',
		0x135: 'z',
		0x136: 'L1',
		0x137: 'R1',
		0x138: 't_l_2',
		0x139: 't_r_2',
		0x13a: 'BACK',
		0x13b: 'START',
		0x13c: 'LOGO',
		0x13d: 'L3',
		0x13e: 'R3',

		0x220: 'd_pad_up',
		0x221: 'd_pad_down',
		0x222: 'd_pad_left',
		0x223: 'd_pad_right',

		# XBox 360 controller uses these codes.
		0x2c0: 'd_pad_left',
		0x2c1: 'd_pad_right',
		0x2c2: 'd_pad_up',
		0x2c3: 'd_pad_down',
	}

	ioctl_cmd = {
		'device_name': 0x80006a13,
		'device_num_buttons': 0x80016a12,
		'device_map_buttons': 0x80406a34,
		'device_num_axes': 0x80016a11,
		'device_map_axes': 0x80406a32,
	}

	def __init__(self, debug=False):

		self.debug = debug

		# We'll store the states here.
		self.axis_states = {}
		self.button_states = {}

		self.axis_map = []
		self.button_map = []

		self.joystick_device = self.open()
		# self.joystick_device = None
		# for fn in os.listdir('/dev/input'):
		# 	if fn.startswith('js'):
		# 		# Open the joystick device.
		# 		fn = '/dev/input/%s' % fn
		# 		self.print('Opening %s...' % fn)
		# 		self.joystick_device = open(fn, 'rb')
		# 		break

		if self.joystick_device:
			# Get the device name.
			buf = array.array('B', [0] * 64)
			ioctl(self.joystick_device, Joystick.ioctl_cmd['device_name'] + (0x10000 * len(buf)), buf)  # JSIOCGNAME(len)
			js_name = buf.tobytes().decode('utf-8')
			self.print('Device name: %s' % js_name)

			# Get number of buttons and axes.
			buf = array.array('B', [0])
			ioctl(self.joystick_device, Joystick.ioctl_cmd['device_num_axes'], buf)  # JSIOCGAXES
			num_axes = buf[0]

			buf = array.array('B', [0])
			ioctl(self.joystick_device, Joystick.ioctl_cmd['device_num_buttons'], buf)  # JSIOCGBUTTONS
			num_buttons = buf[0]

			# Get the axis map.
			buf = array.array('B', [0] * 0x40)
			ioctl(self.joystick_device, Joystick.ioctl_cmd['device_map_axes'], buf)  # JSIOCGAXMAP

			for axis in buf[:num_axes]:
				axis_name = Joystick.axis_names.get(axis, 'unknown(0x%02x)' % axis)
				self.axis_map.append(axis_name)
				self.axis_states[axis_name] = 0.0

			# Get the button map.
			buf = array.array('H', [0] * 200)
			ioctl(self.joystick_device, Joystick.ioctl_cmd['device_map_buttons'], buf)  # JSIOCGBTNMAP

			for btn in buf[:num_buttons]:
				btn_name = Joystick.button_names.get(btn, 'unknown(0x%03x)' % btn)
				self.button_map.append(btn_name)
				self.button_states[btn_name] = 0

			self.print('%d axes found: %s' % (num_axes, ', '.join(self.axis_map)))
			self.print('%d buttons found: %s' % (num_buttons, ', '.join(self.button_map)))
		else:
			self.print('...not found')

	def print(self, msg):
		self.debug and print(msg)

	def open(self):
		self.joystick_device = None
		# Iterate over the joystick devices.
		self.print('Available devices:')
		for fn in os.listdir('/dev/input'):
			if fn.startswith('js'):
				# Open the joystick device.
				fn = '/dev/input/%s' % fn
				self.print('Opening %s...' % fn)
				self.joystick_device = open(fn, 'rb', 0)
			else:
				self.print('No joystick found')
		return self.joystick_device

	def close(self):
		self.joystick_device and self.joystick_device.close()

	def reopen(self):
		self.close()
		self.open()

	@staticmethod
	def singleton(debug):
		if not Joystick.instance:
			dev = Joystick(debug)
			if dev.joystick_device:
				Joystick.instance = dev
		return Joystick.instance

	# Main event loop
	def loop(self, listener=None):
		while self.joystick_device:
			event_buffer = self.joystick_device.read(8)
			if event_buffer:
				time, value, event_type, number = struct.unpack('IhBB', event_buffer)

				if event_type & 0x80:
					self.print("(initial)"),

				if event_type & 0x01:
					button = self.button_map[number]
					if button:
						self.button_states[button] = value
						listener and listener.on_button(button, not not value)
						self.print("%s: %s" % (button, 'pressed' if value else 'released'))

				if event_type & 0x02:
					axis = self.axis_map[number]
					if axis:
						if axis in ['L2', 'R2']:
							normalized_value = value / 32767.0 / 2 + .5
						else:
							normalized_value = value / 32767.0
						self.axis_states[axis] = normalized_value
						listener and listener.on_axis(axis, normalized_value)
						self.print("%s: %.3f" % (axis, normalized_value))

			listener and listener.on_tick()

	def safe_loop(self, listener=None):
		try:
			self.loop(listener)
		except Exception as e:
			print(e)
			print('Trying to restore after failure')
			self.reopen()
			self.safe_loop(listener)


if __name__ == '__main__':
	js = Joystick.singleton(True)
	js.loop()
