#!/usr/bin/python3

from pwm import PWM
import time                # Import necessary modules

pwm_center_value = 430
pwm_radius_value = 100
pwm = None


def setup(offset=0):
	global pwm_center_value, pwm
	pwm_center_value += offset
	pwm = PWM.init()  # Initialize the servo controller.


def steer(angle=0.0):
	global pwm_center_value, pwm_radius_value
	pwm.set_pwm(0, 0, int(pwm_center_value + pwm_radius_value * angle))  # CH0


# ==========================================================================================
# Control the servo connected to channel 0 of the servo control board, so as to make the 
# car turn left.
# ==========================================================================================
def turn_left(angle=1.0):
	steer(-angle)


# ==========================================================================================
# Make the car turn right.
# ==========================================================================================
def turn_right(angle=1.0):
	steer(angle)


# ==========================================================================================
# Make the car turn back.
# ==========================================================================================
def home():
	steer(0.0)


def calibrate(x):
	pwm.set_pwm(0, 0, 450+x)


def test():
	while True:
		turn_left()
		time.sleep(1)
		home()
		time.sleep(1)
		turn_right()
		time.sleep(1)
		home()

if __name__ == '__main__':
	setup()
	turn_left()
	time.sleep(1)
	home()
	time.sleep(1)
	turn_right()
	time.sleep(1)
	home()


