#!/bin/sh
### BEGIN INIT INFO
# Provides:          joystickcard
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       Joystick controlled car as a service
### END INIT INFO

# Change the next 3 lines to suit where you install your script and what you want to call it
DIR=/home/pi/JoystickCar
DAEMON=${DIR}/car.py
DAEMON_NAME=joystickcard

# Add any command line options for your daemon here
DAEMON_OPTS=""

# This next line determines what user the script runs as.
# Root generally not recommended but necessary if you are using the Raspberry Pi GPIO from Python.
DAEMON_USER=root

# The process ID of the script when it runs is stored here:
PIDFILE=/var/run/${DAEMON_NAME}.pid

. /lib/lsb/init-functions

start () {
    log_daemon_msg "Starting system $DAEMON_NAME daemon"
    start-stop-daemon --start --background --pidfile ${PIDFILE} --make-pidfile --user ${DAEMON_USER} --chuid ${DAEMON_USER} --startas ${DAEMON} -- ${DAEMON_OPTS}
    log_end_msg $?
}
stop () {
    log_daemon_msg "Stopping system $DAEMON_NAME daemon"
    start-stop-daemon --stop --pidfile ${PIDFILE} --retry 10
    log_end_msg $?
}

update() {
  su -c "cd $DIR && git pull" ${DAEMON_USER}
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  uninstall)
    uninstall
    ;;
  retart)
    stop
    start
    ;;
  update)
    stop
    update
    start
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|update|uninstall}"
esac