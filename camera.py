#!/usr/bin/python3

from pwm import PWM
from threading import Thread
import time


class CameraControl:
	pulse_min = 200
	pulse_max = 700

	x_min = pulse_min
	x_max = pulse_max
	y_min = pulse_min
	y_max = pulse_max
	x_home = (x_max + x_min) / 2
	y_home = y_min + 80
	x_radius = max(x_home - x_min, x_max - x_home)
	y_radius = max(y_home - y_min, y_max - y_home)

	pwm = None  # Initialize the servo controller.

	x_acceleration = 10
	y_acceleration = 10
	x_inversion = 1
	y_inversion = 1

	def __init__(self, offset_x=0, offset_y=0):
		self.x_current = 0
		self.y_current = 0
		self.x_acceleration = 0.0
		self.y_acceleration = 0.0
		self.thread = Thread(None, self)

		CameraControl.x_min += offset_x
		CameraControl.x_max += offset_x
		CameraControl.y_min += offset_y
		CameraControl.y_max += offset_y
		CameraControl.x_home += offset_x
		CameraControl.y_home += offset_y
		CameraControl.pwm = PWM.init()           # Initialize the servo controller.

	def turn_x(self, value):
		value = max(value, CameraControl.x_min)
		value = min(value, CameraControl.x_max)
		self.x_current = value
		CameraControl.pwm.set_pwm(14, 0, self.x_current)  # CH14 <---> X axis

	def turn_y(self, value):
		value = max(value, CameraControl.y_min)
		value = min(value, CameraControl.y_max)
		self.y_current = value
		CameraControl.pwm.set_pwm(15, 0, self.y_current)  # CH15 <---> Y axis

	def turn_x_angle(self, angle=0.0):
		value = int(CameraControl.x_home + angle * CameraControl.x_radius * CameraControl.x_inversion)
		self.turn_x(value)

	def turn_y_angle(self, angle=0.0):
		value = int(CameraControl.y_home + angle * CameraControl.y_radius * CameraControl.y_inversion)
		self.turn_y(value)

	def turn_xy_angle(self, angle_x=0.0, angle_y=0.0):
		self.turn_x_angle(angle_x)
		self.turn_y_angle(angle_y)

	def turn_x_offset(self, offset=0):
		value = int(CameraControl.x_home + offset * CameraControl.x_inversion)
		self.turn_x(value)

	def turn_y_offset(self, offset=0):
		value = int(CameraControl.y_home + offset * CameraControl.y_inversion)
		self.turn_y(value)

	def turn_xy_offset(self, offset_x=0, offset_y=0):
		self.turn_x_offset(offset_x)
		self.turn_y_offset(offset_y)

	def turn_x_delta(self, speed=0.0):
		value = int(self.x_current + speed * CameraControl.x_acceleration * CameraControl.x_inversion)
		self.turn_x(value)

	def turn_y_delta(self, speed=0.0):
		value = int(self.y_current + speed * CameraControl.y_acceleration * CameraControl.y_inversion)
		self.turn_y(value)

	def turn_xy_delta(self, speed_x=0.0, speed_y=0.0):
		self.turn_x_delta(speed_x)
		self.turn_y_delta(speed_y)

	def turn_x_accelerate(self, speed):
		self.x_acceleration = speed

	def turn_y_accelerate(self, speed):
		self.y_acceleration = speed

	def acceleration_loop(self):
		while True:
			self.turn_xy_delta(self.x_acceleration, self.y_acceleration)

	def __call__(self):
		self.acceleration_loop()

	def start_acceleration_loop(self):
		self.thread.start()

if __name__ == '__main__':
	camera = CameraControl()
	camera.turn_xy_angle(0.0, 0.0)
	time.sleep(0.5)
	for i in range(0, 9):
		camera.turn_xy_delta(1.0, 1.0)
		time.sleep(0.5)
	for i in range(0, 9):
		camera.turn_xy_delta(-1.0, -1.0)
		time.sleep(0.5)
	camera.turn_xy_angle(0.0, 0.0)
