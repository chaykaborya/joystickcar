#!/usr/bin/python3

import RPi.GPIO as GPIO

import time


class LightControl:

	# So that you don't need to manage non-descriptive numbers,
	# set "LIGHT" to 31 so that our code can easily reference the correct pin.
	LIGHT = 31
	state = False

	def __init__(self):

		GPIO.setwarnings(False)

		# Set the pin designation type.
		GPIO.setmode(GPIO.BOARD)

		# Because GPIO pins can act as either digital inputs or outputs,
		# we need to designate which way we want to use a given pin.
		# This allows us to use functions in the GPIO library in order to properly send and receive signals.
		GPIO.setup(self.LIGHT, GPIO.OUT)

	def switch(self, is_active):
		GPIO.output(self.LIGHT, is_active)
		self.state = is_active

	def toggle(self):
		self.switch(not self.state)

if __name__ == '__main__':
	lights = LightControl()
	lights.switch(False)
	time.sleep(0.5)
	for i in range(0, 3):
		lights.toggle()
		time.sleep(0.5)
	lights.switch(False)
