#!/usr/bin/python3

import RPi.GPIO as GPIO
import math
from pwm import PWM
import time  # Import necessary modules

# ===========================================================================
# Raspberry Pi pin11, 12, 13 and 15 to realize the clockwise/counterclockwise
# rotation and forward and backward movements
# ===========================================================================
Motor0_A = 11  # pin11
Motor0_B = 12  # pin12
Motor1_A = 13  # pin13
Motor1_B = 15  # pin15

# ===========================================================================
# Set channel 4 and 5 of the servo driver IC to generate PWM, thus 
# controlling the speed of the car
# ===========================================================================
EN_M0 = 4  # servo driver IC CH4
EN_M1 = 5  # servo driver IC CH5

pins = [Motor0_A, Motor0_B, Motor1_A, Motor1_B]

p = PWM.init()


def setup():
	GPIO.setwarnings(False)
	GPIO.setmode(GPIO.BOARD)  # Number GPIOs by its physical location
	for pin in pins:
		GPIO.setup(pin, GPIO.OUT)  # Set all pins' mode as output


# ===========================================================================
# Adjust the duty cycle of the square waves output from channel 4 and 5 of
# the servo driver IC, so as to control the speed of the car.
# ===========================================================================
def set_speed_split(speed_left=0.0, speed_right=0.0):
	speed_left = min(speed_left, 1.0)
	speed_left = max(speed_left, -1.0)
	speed_right = min(speed_right, 1.0)
	speed_right = max(speed_right, -1.0)
	p.set_pwm(EN_M0, 0, int(speed_left * 4095))
	p.set_pwm(EN_M1, 0, int(speed_right * 4095))


def set_speed(speed=0.0):
	set_speed_split(speed, speed)


# ===========================================================================
# Control the DC motor to make it rotate clockwise, so the car will 
# move forward.
# ===========================================================================
def set_direction_split(direction_left=0.0, direction_right=0.0):  # -1 (rev 10) 0 (stop 00) 1 (forward 01)
	GPIO.output(Motor0_A, GPIO.LOW if direction_left <= 0 else GPIO.HIGH)
	GPIO.output(Motor0_B, GPIO.LOW if direction_left >= 0 else GPIO.HIGH)
	GPIO.output(Motor1_A, GPIO.LOW if direction_right <= 0 else GPIO.HIGH)
	GPIO.output(Motor1_B, GPIO.LOW if direction_right >= 0 else GPIO.HIGH)


def set_direction(d=0.0):
	set_direction_split(d, d)


def set_direction_speed_split(speed_left=0.0, speed_right=0.0):
	set_direction_split(speed_left, speed_right)
	set_speed_split(abs(speed_left), abs(speed_right))


def set_direction_speed(speed=0.0):
	set_direction_speed_split(speed, speed)


def forward():
	set_direction(1)


def backward():
	set_direction(-1)


def stop():
	set_direction(0)


# ===========================================================================
# The first parameter(status) is to control the state of the car, to make it 
# stop or run. The parameter(direction) is to control the car's direction 
# (move forward or backward).
# ===========================================================================
def ctrl(status, direction=1):
	if status == 1:  # Run
		if direction == 1:  # Forward
			forward()
		elif direction == -1:  # Backward
			backward()
		else:
			print('Argument error! direction must be 1 or -1.')
	elif status == 0:  # Stop
		stop()
	else:
		print('Argument error! status must be 0 or 1.')


def test():
	while True:
		setup()
		ctrl(1)
		time.sleep(3)
		set_speed(10)
		time.sleep(3)
		set_speed(100)
		time.sleep(3)
		ctrl(0)


if __name__ == '__main__':
	setup()
	set_speed(0.5)
	set_direction(1)
	time.sleep(3)
	set_speed(10)
	set_direction(-1)
	time.sleep(3)
	set_direction(0)
	# forward()
	# backward()
	stop()
